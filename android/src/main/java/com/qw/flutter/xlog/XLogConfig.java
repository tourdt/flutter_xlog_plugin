package com.qw.flutter.xlog;

/**
 * @date: 2021/02/24
 * @author: lixu
 * @description: 日志配置参数
 */
public class XLogConfig {
    /**
     * 日志文件前缀
     */
    public String logFileNamePrefix = "xlog";
    /**
     * 控制台是否打印日志
     */
    public boolean isConsoleLogOpen = true;
    /**
     * 日志tag
     */
    public String logTag = "BaseLibTag";
    /**
     * 日志保存路径
     * 为null，保存到内部存储路径下
     */
    public String logPath;
    /**
     * 是否打印线程名
     */
    public boolean isPrintThreadName = true;
    /**
     * 日志文件保存到本地，加密公钥
     * 为空表示日志文件不加密
     * 日志解密参考readme文件
     */
    public String encryptPubKey;

    public XLogConfig() {
    }
}
