package com.qw.flutter.xlog;


/**
 * 创建时间: 2020/5/21 16:49
 * 作者:lixu
 * 功能描述:xlog日志打印
 */
public class XXLog {

    public static void v(String tag, String msg) {
        XLogWrapper.getInstance().printLog("v", tag, msg);
    }

    public static void i(String tag, String msg) {
        XLogWrapper.getInstance().printLog("i", tag, msg);
    }

    public static void d(String tag, String msg) {
        XLogWrapper.getInstance().printLog("d", tag, msg);
    }

    public static void w(String tag, String msg) {
        XLogWrapper.getInstance().printLog("w", tag, msg);
    }

    public static void e(String tag, String msg) {
        XLogWrapper.getInstance().printLog("e", tag, msg);
    }

}
