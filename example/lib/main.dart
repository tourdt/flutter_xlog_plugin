import 'package:flutter/material.dart';
import 'package:flutter_xlog_plugin/xlog_lib.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();

    //TODO  测试，在初始化前打印日志；
    XLogUtils.e("hahaha", null);
    XLogUtils.e("hahaha", "这是初始化前打印的日志");

    XLogUtils.init(
      tag: "rishli",
      isConsolePrintLog: true,
      saveLogFilePath: '/storage/emulated/0/flutter_xlog_app/xlog/',
      encryptPubKey: null,
      onIosInitCallback: () {
        ///TODO iOS需要完善xlog初始化逻辑
      },
      onIosLogCallback: (String level, String tag, String msg) {
        ///TODO iOS需要完善log打印逻辑
      },
      onIosAppenderFlushCallback: () {
        ///TODO iOS需要完善appenderFlush逻辑
      },
      onIosDisposeCallback: () {
        ///TODO iOS需要完善dispose逻辑
      },
    );
  }

  int count = 0;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    XLogUtils.appenderFlush();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: RaisedButton(
          child: Text('点击打印日志'),
          onPressed: () {
            count++;
            XLogUtils.e("MyApp", null);
            XLogUtils.e("MyApp", '当前是第：$count条日志');
            XLogUtils.i("MyApp", '当前是第：$count条日志');
            XLogUtils.d("MyApp", '当前是第：$count条日志');
            XLogUtils.w("MyApp", '当前是第：$count条日志');
            XLogUtils.v("MyApp", '当前是第：$count条日志');
            XLogUtils.v("MyApp", '同步日志考虑到效率问题，不使用加密。也不建议给线上用户开启同步日志，推荐所有 release 版本异步模式日志');
          },
        ),
      ),
    );
  }
}
