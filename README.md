# flutter_xlog_plugin

基于[Mars xlog](https://mp.weixin.qq.com/s/cnhuEodJGIbdodh0IxNeXQ)封装的flutter日志库，目前仅Android平台才能使用xlog native打印保存日志；
其它平台使用Flutter print()方法打印日志到控制台，不保存日志文件；

[xlog 日志解密参考](https://github.com/Tencent/mars/wiki/Xlog-%E5%8A%A0%E5%AF%86%E4%BD%BF%E7%94%A8%E6%8C%87%E5%BC%95)

## Android日志说明

* 只要调用了XLogUtils类打印的日志(不区分日志级别)都会保存到手机目录中；  
* log文件保存目录为开发者初始化时指定；  

* 一、开启log日志文件加密流程：
    1. 通过[mars/log/crypt/gen_key.py 脚本](https://github.com/Tencent/mars/blob/master/mars/log/crypt/gen_key.py)生成公钥和私钥；
    2. 用第1步生成的公钥和私钥替换[mars/log/crypt/decode_mars_crypt_log_file.py脚本](https://github.com/Tencent/mars/blob/master/mars/log/crypt/decode_mars_crypt_log_file.py)中的钥匙，这个脚本用于后面log文件解密；
    3. 在代码中初始化xlog模块时，传递第1步生成的公钥给sdk；
    4. 解密：使用第2步替换后的脚本解密；
    *** 

* 二、不开启log文件加密流程：
    1. 在代码中初始化xlog模块时，公钥传递空字符；
    2. 使用[mars/log/crypt/decode_mars_nocrypt_log_file.py脚本](https://github.com/Tencent/mars/blob/master/mars/log/crypt/decode_mars_nocrypt_log_file.py)解压；
    *** 

* 三、log解密方式:
    1. 日志开启了加密：
    python2 decode_mars_crypt_log_file.py(解密脚本，修改为自己生成的公钥和私钥) xlog_xxxx.xlog(日志文件名)

    2. 日志未开启加密：
    python2 decode_mars_nocrypt_log_file.py(解密脚本，通用) xlog_xxxx.xlog(日志文件名)

* 解密脚本存放路径:工程目录/logdoc/decode_mars_xxx_log_file.py
***


## 使用方式
```
dependencies:
    flutter_xlog_plugin:
        git:
            url: https://gitee.com/rishli/flutter_xlog_plugin.git
            ref: (tag标签)
```